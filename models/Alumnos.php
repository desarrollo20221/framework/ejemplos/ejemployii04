<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $codigo
 * @property string $nombre
 * @property string $apellido1
 * @property string $apellido2
 * @property string $direccion
 * @property string $poblacion
 * @property string $fecha_nacimiento
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido1', 'apellido2', 'direccion', 'poblacion', 'fecha_nacimiento'], 'required'],
            [['fecha_nacimiento'], 'safe'],
            [['nombre', 'apellido1', 'apellido2', 'poblacion'], 'string', 'max' => 100],
            [['direccion'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo del alumno',
            'nombre' => 'Nombre del alumno',
            'apellido1' => 'Primer apellido ',
            'apellido2' => 'Segundo apellido',
            'direccion' => 'Direccion',
            'poblacion' => 'Poblacion',
            'fecha_nacimiento' => 'Fecha Nacimiento',
        ];
    }
}
